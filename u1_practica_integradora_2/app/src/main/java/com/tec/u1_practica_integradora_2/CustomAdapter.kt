package com.tec.u1_practica_integradora_2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_user_list.view.*

class CustomAdapter(private val listener: (User, Int) -> Unit): RecyclerView.Adapter<CustomAdapterViewHolder>() {

    private var list: MutableList<User> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomAdapterViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_user_list, parent, false)
        return CustomAdapterViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: CustomAdapterViewHolder, position: Int) {
        holder.setData(list[position], position, listener)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    //PARA AGREGAR VARIOS ITEMS
    fun setList(list: List<User>){
        this.list.addAll(list)
    }

    //PARA AGREGAR UNA SOLA (NO SE USA EN ESTA PRACTICA)
    fun addPerson(persona: User){
        this.list.add(persona)
        notifyItemInserted(list.size-1)
    }

}

class CustomAdapterViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

    fun setData(persona: User, position: Int, listener: (User, Int) -> Unit){
        itemView.apply {
            //ASIGNACION DE VALORES A LA VISTA DE CADA ITEM DE LA LISTA
            tvNameAge.text = "${persona.name} ${persona.lastName} (${persona.age})"
            tvOrganization.text = "Organización: ${persona.organization}"
            tvEmail.text = "Email: ${persona.email}"
            tvGender.text = if (persona.gender){ //ES HOMBRE
                tvGender.setTextColor(ContextCompat.getColor(context, R.color.male))
                "H"
            } else{ //ES MUJER
                tvGender.setTextColor(ContextCompat.getColor(context, R.color.female))
                "M"
            }
            setOnClickListener {
                listener.invoke(persona, position)
            }
        }
    }

}