package com.tec.u1_practica6_intentos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import kotlinx.android.synthetic.main.activity_main.*

const val TEXT_EXTRA = "text_extra"

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnGo.setOnClickListener {
            //val bundle = Bundle()
            val intent = Intent(this, OtherActivity::class.java)
            intent.putExtra(TEXT_EXTRA, etTexto.text.toString())
            val pair: Pair<View, String> = Pair.create(ivImage, "image")
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, pair)
            startActivity(intent, options.toBundle())
        }
    }
}