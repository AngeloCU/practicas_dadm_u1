package com.tec.u1_practica3_layouts_y_componentes

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnShow.setOnClickListener {
            if (etName.text.isNotEmpty() && etLastName.text.isNotEmpty() && etAge.text.isNotEmpty()){
                val persona = when {
                    rbMen.isChecked -> {
                        Persona(etName.text.toString(), etLastName.text.toString(), etAge.text.toString().toInt(), "Hombre")
                    }
                    rbWoman.isChecked -> {
                        Persona(etName.text.toString(), etLastName.text.toString(), etAge.text.toString().toInt(), "Mujer")
                    }
                    else -> {
                        Persona(etName.text.toString(), etLastName.text.toString(), etAge.text.toString().toInt(), "No especifica")
                    }
                }

                Toast.makeText(this, "Persona de nombre: ${persona.name} ${persona.lastName}\n" +
                        "Edad: ${persona.age}\nDe genero: ${persona.gender}", Toast.LENGTH_LONG).show()

            } else {
                Toast.makeText(this, "Por favor rellena la información solicitada", Toast.LENGTH_SHORT).show()
            }
        }
    }
}

data class Persona(var name: String, var lastName: String, var age: Int, var gender: String)