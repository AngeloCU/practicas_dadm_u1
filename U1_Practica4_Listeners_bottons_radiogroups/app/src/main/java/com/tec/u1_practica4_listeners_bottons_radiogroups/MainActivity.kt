package com.tec.u1_practica4_listeners_bottons_radiogroups

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.RadioButton
import android.widget.Toast
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnListener.setOnClickListener {
            Toast.makeText(this, "Boton por listener", Toast.LENGTH_SHORT).show()
        }

        btnClase.setOnClickListener(this)

        rgGroup.setOnCheckedChangeListener { group, checkedId ->
            when(group.findViewById<RadioButton>(checkedId)){
                rbBlack -> {
                    Toast.makeText(this, "Esta marcado negro", Toast.LENGTH_SHORT).show()
                    root.setBackgroundColor(Color.BLACK)
                    rbBlack.setTextColor(ContextCompat.getColor(this, R.color.white))
                    rbWhite.setTextColor(ContextCompat.getColor(this, R.color.white))
                    rbOther.setTextColor(ContextCompat.getColor(this, R.color.white))
                }
                rbWhite -> {
                    Toast.makeText(this, "Esta marcado blanco", Toast.LENGTH_SHORT).show()
                    root.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
                    rbBlack.setTextColor(Color.BLACK)
                    rbWhite.setTextColor(Color.BLACK)
                    rbOther.setTextColor(Color.BLACK)
                }
                rbOther -> {
                    Toast.makeText(this, "Esta marcado otro", Toast.LENGTH_SHORT).show()
                    root.setBackgroundColor(ContextCompat.getColor(this, R.color.ak))
                }
            }
        }

    }

    fun onXmlClick(view: View){
        Toast.makeText(this, "Boton por xml", Toast.LENGTH_SHORT).show()
        if (rbBlack.isChecked){
            rbBlack.setTextColor(ContextCompat.getColor(this, R.color.white))
            rbWhite.setTextColor(ContextCompat.getColor(this, R.color.white))
            rbOther.setTextColor(ContextCompat.getColor(this, R.color.white))
        }
    }

    override fun onClick(v: View?) {
        Toast.makeText(this, "Boton por Clase", Toast.LENGTH_SHORT).show()
        /*val button = v as Button
        when(button.id){
            R.id.btnListener -> {
                Toast.makeText(this, "BtnListener", Toast.LENGTH_SHORT).show()
            }
            R.id.btnClase -> {
                Toast.makeText(this, "btn Clase", Toast.LENGTH_SHORT).show()
            }
            R.id.btnXml -> {
                Toast.makeText(this, "btn xml", Toast.LENGTH_SHORT).show()
            }
        }*/
    }
}