package com.tec.u1_practica9_landscape_portrait

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.widget.Toast
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var index = 0
    private val list = mutableListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Toast.makeText(this, "onCreate", Toast.LENGTH_SHORT).show()

        list.add("https://www.residentevil.com/village/assets/images/common/share.png")
        list.add("https://www.infobae.com/new-resizer/Klb8EaAhYRg8-YLzgLIFNzRb3KQ=/420x236/filters:format(jpg):quality(85)/cloudfront-us-east-1.images.arcpublishing.com/infobae/MCDA7Y32QBDETKDZK5JEZCTLR4.jpg")
        list.add("https://cdn.hobbyconsolas.com/sites/navi.axelspringer.es/public/media/image/2021/01/resident-evil-village-2206645.jpeg")
        list.add("https://www.residentevil.com/village/assets/images/topics/thmub-210122-03.jpg")
        list.add("https://www.somosxbox.com/wp-content/uploads/2021/03/Resident-Evil-Village-revela-sus-requisitos-minimos-y-recomendados-en-PC-min-790x444.jpg?mrf-size=m")
        val count = list.size

        btnPrev.setOnClickListener {
            if (index > 0){
                index--
                val image = list[index]
                Picasso.get().load(image).into(imageView)
            }
        }

        btnNext.setOnClickListener {
            if (index < count - 1){
                index++
                val image = list[index]
                Picasso.get().load(image).into(imageView)
            }
        }
    }

    override fun onStart() {
        super.onStart()
        Toast.makeText(this, "onStart", Toast.LENGTH_SHORT).show()
    }

    override fun onResume() {
        super.onResume()
        Toast.makeText(this, "onResume", Toast.LENGTH_SHORT).show()
    }

    override fun onPause() {
        super.onPause()
        Toast.makeText(this, "onPause", Toast.LENGTH_SHORT).show()
    }

    override fun onStop() {
        super.onStop()
        Toast.makeText(this, "onStop", Toast.LENGTH_SHORT).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        Toast.makeText(this, "onDestroy", Toast.LENGTH_SHORT).show()
    }

    override fun onRestart() {
        super.onRestart()
        Toast.makeText(this, "onRestart", Toast.LENGTH_SHORT).show()
    }
}