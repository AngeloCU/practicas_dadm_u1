package com.tec.u1_practica1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        tvText.text = "CHA CHA CHA"

        tvText.setOnClickListener {
            Toast.makeText(this, "HOLA!", Toast.LENGTH_SHORT).show()
        }

        val valor: Int? = null
        val valor2 = 0
        val lolo = 0

        valor?.let { notNullValue ->
            suma(notNullValue, valor2)
        }

        val p1 = Person("LALO LANDA", 45)
        tvText.text = "Persona1: ${p1.name}, ${p1.age}"

    }

    fun suma(valor: Int, valor2: Int){

    }
}

data class Person(var name: String, var age: Int)