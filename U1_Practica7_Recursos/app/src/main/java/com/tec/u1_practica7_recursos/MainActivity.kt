package com.tec.u1_practica7_recursos

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnRes.setOnClickListener {
            ivImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.imagen2))
        }

        btnUrl.setOnClickListener {
            Picasso.get()
                .load("https://firebasestorage.googleapis.com/v0/b/triploop-dev.appspot.com/o/test%2FAngTest%2Ffdac40ff3960d5f482bce57c78402df3ba519661.jpg?alt=media&token=ce87e56f-7d87-48cd-bc4c-128d45d6d200")
                .into(ivImage)
        }

    }
}